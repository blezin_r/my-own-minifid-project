# my own minifid

Le projet suivant se divise en 3 grandes étapes :

1.  Création du projet Django (v2.x, avec Python3.6.x)
2.  Création d’un API avec Django Rest Framework
3.  Déploiement sur AWS

### REQUIS :

-   Projet réalisé entièrement en anglais (nom de variable, commentaires, commit...).
    
-   Versionner le projet sur bitbucket.
    
-   Projet dans un environnement virtualenv.
    
-   Le projet doit à terme être sur une instance AWS ECS2. (Supervisor, Gunicorn)
    
-   La base de donnée du projet à terme doit être sur AWS RDS. (PostgreSQL)
    
#### A. Création du projet Django (v2.x, avec Python3.6.x) :
    

1.  Représenter le model de donnée en UML avant de commencer à taper la moindre ligne de code.

  

2.  Créer les models suivant :
    

-   User
    
-   Contact
    
-   Bill
    
-   Bill_product
    
-   Product
    
-   Shop
    

  

3.  Créer les vues associées à ces models
    

(C.R.U.D non auto-généré par Django).

  

4.  Créer les pages de login et logout pour les Users
    

  

#### B.  Création d’une API avec Django Rest Framework
  
1.  Créer les breakpoints suivant :
    

-   Lister tous les tickets (Bill)
    

-   Facultatif : Avec les lignes de produits associées (Bill_product)
    

-   Créer un ticket (Bill)
    
-   Modifier un ticket (Bill)
    
-   Lister tous les contacts (Contact)
    

  

2.  Ajouter une couche de sécurité OAUTH2 pour utiliser de l'extérieur l’API
    

#### C. Déploiement sur AWS
Disponible à l'adresse suivante : https://minifid.dev-lineup7.fr