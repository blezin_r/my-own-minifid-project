from django.db import models
from django.urls import reverse


class Shop(models.Model):
    """
    Implements shops
    """
    name = models.CharField(max_length=30)
    address = models.CharField(max_length=200)

    def __str__(self):
        return str(self.name)


class Contact(models.Model):
    """
    Implements contacts
    """
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)

    def get_absolute_url(self):
        return reverse('managebill:contacts')

    def __str__(self):
        return str(self.first_name)


class Product(models.Model):
    """
    Implements products
    """
    name = models.CharField(max_length=200)

    def __str__(self):
        return str(self.name)


class Bill(models.Model):
    """
    Implements bills
    """
    date = models.DateField(auto_now_add=True)
    ticket_number = models.IntegerField(unique=True)
    contact = models.ForeignKey(Contact, on_delete=models.CASCADE)
    shop = models.ForeignKey(Shop, on_delete=models.CASCADE)
    products = models.ManyToManyField(Product, related_name='bills', through='BillProduct')

    def __str__(self):
        return str(self.ticket_number)


class BillProduct(models.Model):
    """
    Implements bill products
    """
    bill = models.ForeignKey(Bill, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField()

    def __str__(self):
        return str(self.bill)
