from django.contrib import admin
from .models import Contact, Bill, BillProduct, Product, Shop


admin.site.register(Contact)
admin.site.register(Bill)
admin.site.register(BillProduct)
admin.site.register(Product)
admin.site.register(Shop)
