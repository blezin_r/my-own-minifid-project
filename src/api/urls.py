from rest_framework import routers
from django.urls import include, path

from .views import (
    BillCreateAPIView,
    BillDeleteAPIView,
    BillDetailAPIView,
    BillListAPIView,
    ContactListAPIView,
    BillUpdateAPIView,
)

router = routers.DefaultRouter()

# Default routes
urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]

# Personal routes
urlpatterns += [
    path('bills/', BillListAPIView.as_view(), name='bill_list'),
    path('bills/create/', BillCreateAPIView.as_view(), name='bill_create'),
    path('bills/<int:id>/delete/', BillDeleteAPIView.as_view(), name='bill_delete'),
    path('bills/<int:id>/details/', BillDetailAPIView.as_view(), name='bill_list'),
    path('bills/<int:id>/edit/', BillUpdateAPIView.as_view(), name='bill_update'),
    path('contacts/', ContactListAPIView.as_view(), name='contact_list'),
]
