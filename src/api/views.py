from rest_framework.generics import (
    CreateAPIView,
    DestroyAPIView,
    ListAPIView,
    RetrieveAPIView,
    RetrieveUpdateAPIView,
)
from rest_framework.permissions import IsAuthenticated


from managebill.models import Bill, Contact
from .serializers import (
    BillCreateSerializer,
    BillSerializer,
    BillUpdateSerializer,
    ContactSerializer
)


class BillCreateAPIView(CreateAPIView):
    """
    Bill create view
    Extends:
        CreateAPIView
    """
    permission_classes = (IsAuthenticated,)
    queryset = Bill.objects.all()
    serializer_class = BillSerializer


class BillDeleteAPIView(DestroyAPIView):
    """
    Bill delete view
    Extends:
        DestroyAPIView
    """
    permission_classes = (IsAuthenticated,)
    lookup_field = 'ticket_number'
    lookup_url_kwarg = 'id'
    queryset = Bill.objects.all()
    serializer_class = BillSerializer


class BillDetailAPIView(RetrieveAPIView):
    """
    Bill detail view
    Extends:
        RetrieveAPIView
    """
    permission_classes = (IsAuthenticated,)
    lookup_field = 'ticket_number'
    lookup_url_kwarg = 'id'
    queryset = Bill.objects.all()
    serializer_class = BillSerializer


class BillListAPIView(ListAPIView):
    """
    Bill list view
    Extends:
        ListAPIView
    """
    permission_classes = (IsAuthenticated,)
    queryset = Bill.objects.all()
    serializer_class = BillSerializer


class BillUpdateAPIView(RetrieveUpdateAPIView):
    """
    Bill update view
    Extends:
        UpdateAPIView
    """
    permission_classes = (IsAuthenticated,)
    lookup_field = 'ticket_number'
    lookup_url_kwarg = 'id'
    queryset = Bill.objects.all()
    serializer_class = BillUpdateSerializer


class ContactListAPIView(ListAPIView):
    """
    Contact list view
    Extends:
        ListAPIView
    """
    permission_classes = (IsAuthenticated,)
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer
